#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, GLib, Gio, GdkPixbuf
import signal
import gettext
import re
gettext.install("app-select", "/usr/share/locale")
        
class Error:
    def __init__(self, error):
        dlg = Gtk.MessageDialog(parent=None, flags=0, message_type=Gtk.MessageType.ERROR, buttons=Gtk.ButtonsType.OK, text="Error")
        dlg.set_title(_("Failed to updated"))
        dlg.format_secondary_text(error)
        dlg.run()
        dlg.destroy()

class mainWindow(Gtk.Window):
    def apply(self,item,app_file):
        for row in self.store:
            if row[2] == True:
                mime_type = row[0].strip(" ")
                try: 
                    Gio.AppInfo.set_as_default_for_type(Gio.DesktopAppInfo.new_from_filename(app_file), mime_type)
                except:
                    Error(self, _("Could not set %s as default for %s" % app_file, mime_type))
        self.close()
    
    def on_cell_toggled(self, renderertoggle, treepath):
        self.store[treepath][2] = not self.store[treepath][2]

    def __init__(self,mime_list,app_file,app_name):
        Gtk.Window.__init__(self)
        self.set_size_request(640,480)
        self.set_border_width(10)
        self.set_title(_(" App Select - mime editor"))
        self.show()
        
        window_grid = Gtk.Grid()
        self.add(window_grid)
        window_grid.show()
        
        sw = Gtk.ScrolledWindow()
        sw.set_hexpand(True)
        sw.set_vexpand(True)
        window_grid.attach(sw, 1,1,1,1)
        sw.show()
        
        self.store = Gtk.ListStore(str,str,bool)
        for i, item in enumerate(re.split(';', mime_list)):
            default_app = Gio.AppInfo.get_default_for_type(re.sub(' ', '', item), False).get_name()
            self.store.append([item, default_app, False])# self.mime_dictionary[item]])
            #for item in Gio.AppInfo.get_all_for_type(re.sub(' ', '', item)):
            #    print ("optional app: "+item.get_name())
        
        self.treeview = Gtk.TreeView()
        self.treeview.set_model(self.store)
        self.treeview.set_enable_search(False)
        for i, column_title in enumerate([_("Mime Type"), _("Current default"),_("Set %s as default for mime type" % app_name)]):
            if i == 2:
                renderertoggle = Gtk.CellRendererToggle()
                renderertoggle.connect("toggled", self.on_cell_toggled)
                column = Gtk.TreeViewColumn(column_title, renderertoggle, active=2)
            else:
                column = Gtk.TreeViewColumn(column_title, Gtk.CellRendererText(), text=i)
            column.set_visible(True)
            self.treeview.append_column(column)
        
        self.treeview.set_headers_visible(True)
        sw.add(self.treeview)
        self.treeview.show()
        
        select = Gtk.Button.new_from_icon_name("gtk-apply", Gtk.IconSize(1))
        select.connect("clicked", self.apply, app_file)
        window_grid.attach(select, 1,2,2,1) 
        select.set_can_default(True)
        select.grab_default()
        select.show()
        
        self.connect("delete-event", Gtk.main_quit)
        Gtk.main()
